//
//  ModelTypeViewModel.swift
//  CarMart
//
//  Created by Adithi Bolar on 3/20/17.
//  Copyright © 2017 Adithi Bolar. All rights reserved.
//

import Foundation

protocol ReloadModelTableViewDelegate: class {
    func reloadTableView()
}

class ModelTypeViewModel {
    var objects:[ModelCellData] = []
    weak var reloadDelegate:ReloadModelTableViewDelegate?
    var dataDict:NSDictionary = [:]
    
}

extension ModelTypeViewModel: ModelCellDelegate {
    enum JSONError: String, Error {
        case NoData = "ERROR: no data"
        case ConversionFailed = "ERROR: conversion from JSON failed"
    }
}

extension ModelTypeViewModel {
    func gettingModelData(manId: String, page : Int) {
        let urlString = "http://api-aws-eu-qa-1.auto1-test.com/v1/car-types/main-types?manufacturer=\(manId)&page=\(page)&pageSize=10&wa_key=coding-puzzle-client-449cc9d"
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with:url!) { (data, response, error) in
            if error != nil {
                print(error ?? "hello")
            } else {
                do {
                    
                    let parsedData = try JSONSerialization.jsonObject(with: data!, options: []) as! [NSString:Any]
                    self.dataDict = parsedData as NSDictionary
                    
                    print(self.dataDict["wkda"] ?? "Error")
                    
                    let dict = self.dataDict["wkda"] as! NSDictionary
                    
                    for (key, value) in dict {
                        print("key is - \(key) and value is - \(value)")
                        
                        guard let manName = value as? String else {
                            print("Error can't get country name")
                            return
                        }
                        
                        guard let manId = key as? String else {
                            print("Error can't get city name")
                            return
                        }
                        
                        let obj = ModelCellData(modelName: manName, modelId: manId)
                        self.objects.append(obj)
                    }
                    
                    DispatchQueue.main.async {
                        self.reloadDelegate?.reloadTableView()
                    }
                    
                } catch let error as NSError {
                    print(error)
                }
            }
            
            }.resume()
    }
}
