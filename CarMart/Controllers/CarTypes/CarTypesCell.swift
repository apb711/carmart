//
//  CarTypesCell.swift
//  CarMart
//
//  Created by Adithi Bolar on 3/20/17.
//  Copyright © 2017 Adithi Bolar. All rights reserved.
//

import UIKit

protocol ModelCellDataSource {
    var modelName:String { get }
    var modelId:String { get }
}


protocol ModelCellDelegate: class {
}

struct ModelCellData:ModelCellDataSource {
    var modelName: String
    var modelId: String
}

class CarTypesCell: UITableViewCell {
    @IBOutlet weak var modelName: UILabel!
    
    private var dataSource: ModelCellDataSource?
    private weak var delegate: ModelCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(dataSource: ModelCellDataSource, delegate: ModelCellDelegate) {
        self.dataSource = dataSource
        self.delegate = delegate
        
        modelName.text = dataSource.modelName
    }

}
