//
//  MainVC.swift
//  CarMart
//
//  Created by Adithi Bolar on 3/20/17.
//  Copyright © 2017 Adithi Bolar. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UITableViewDataSource, UITableViewDelegate, ReloadTableViewDelegate {
    
    var firstLaunch:Bool = true
    
    var viewModel:TableViewViewModel = TableViewViewModel()
    var viewModelObjectsArrayCopy:[TableViewCellData] = []
    var page:Int = 0
    var totalPages:Int = 8
    var totalManufacturers:Int = 0
    
    @IBOutlet weak var myTabelView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Manufacturers"
        self.page = 1
        
        myTabelView.delegate = self
        myTabelView.dataSource = self
        
        viewModel.reloadDelegate = self
        viewModel.gettingData(page: 0)
        
        myTabelView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.page < self.totalPages) {
            return viewModel.objects.count + 1;
        }
        return viewModel.objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row < viewModel.objects.count) {
            return itemCellForIndexPath(indexPath: indexPath as NSIndexPath);
        } else {
            return loadingCell();
        }
    }
    
    func loadingCell() -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: nil)

        let activityView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityView.center = cell.center;
        cell.addSubview(activityView);
        activityView.startAnimating();
        
        cell.tag = 420;
        
        return cell;
    }
    
    func colorForIndex(index: Int) -> UIColor {
        let itemCount = viewModel.objects.count - 1
        let color = (CGFloat(index) / CGFloat(itemCount)) * 0.6
        return UIColor(red: 1.0, green: color, blue: 0.0, alpha: 1.0)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = colorForIndex(index: indexPath.row)

        if (cell.tag == 420) {
            self.page += 1;
            viewModel.gettingData(page: self.page);
        }
    }

    func itemCellForIndexPath(indexPath: NSIndexPath) -> UITableViewCell {
        let cell = myTabelView.dequeueReusableCell(withIdentifier: "ModelTableViewCell", for: indexPath as IndexPath) as! ModelTableViewCell
        cell.configure(dataSource: (self.viewModel.objects[indexPath.row]), delegate: viewModel)
        
        return cell
    }
    
    func reloadTabelView() {
        self.myTabelView.reloadData()
        if firstLaunch {
            viewModelObjectsArrayCopy = viewModel.objects
            firstLaunch = false
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.row)!")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CarTypesVC") as! CarTypesVC
        let car = self.viewModel.objects[indexPath.row]
        
        nextViewController.manufacturerId = car.manufacturerId
        nextViewController.manufacturerName = car.manufacturerName
        
        self.navigationController?.pushViewController(nextViewController, animated:true)
    }
}
