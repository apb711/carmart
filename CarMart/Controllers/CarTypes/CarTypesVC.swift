//
//  CarTypesVC.swift
//  CarMart
//
//  Created by Adithi Bolar on 3/20/17.
//  Copyright © 2017 Adithi Bolar. All rights reserved.
//

import UIKit

class CarTypesVC: UIViewController, UITableViewDataSource, UITableViewDelegate, ReloadModelTableViewDelegate {
    
    var firstLaunch:Bool = true
    
    var viewModel:ModelTypeViewModel = ModelTypeViewModel()
    var viewModelObjectsArrayCopy:[ModelCellData] = []
    var manufacturerId = String()
    var manufacturerName = String()
    var page:Int = 0
    var totalPages:Int = 1
    
    @IBOutlet weak var modelTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = manufacturerName
        modelTableView.delegate = self
        modelTableView.dataSource = self
        
        viewModel.reloadDelegate = self
        viewModel.gettingModelData(manId: manufacturerId, page: 0)
        
        modelTableView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.page < self.totalPages) {
            return viewModel.objects.count + 1;
        }
        return viewModel.objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row < viewModel.objects.count) {
            return itemCellForIndexPath(indexPath: indexPath as NSIndexPath);
        } else {
            return loadingCell();
        }
    }
    
    func loadingCell() -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: nil)
        
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityView.center = cell.center;
        cell.addSubview(activityView);
        activityView.startAnimating();
        
        cell.tag = 420;
        
        return cell;
    }
    
    func colorForIndex(index: Int) -> UIColor {
        let itemCount = viewModel.objects.count - 1
        let color = (CGFloat(index) / CGFloat(itemCount)) * 0.6
        return UIColor(red: 1.0, green: color, blue: 0.0, alpha: 1.0)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = colorForIndex(index: indexPath.row)
        
        if (cell.tag == 420) {
            self.page += 1;
            viewModel.gettingModelData(manId: manufacturerId, page: self.page);
        }
    }

    func itemCellForIndexPath(indexPath: NSIndexPath) -> UITableViewCell {
        let cell = modelTableView.dequeueReusableCell(withIdentifier: "CarTypesCell", for: indexPath as IndexPath) as! CarTypesCell
        cell.configure(dataSource: (self.viewModel.objects[indexPath.row]), delegate: viewModel)
        
        return cell
    }
    
    func reloadTableView() {
        self.modelTableView.reloadData()
        if firstLaunch {
            viewModelObjectsArrayCopy = viewModel.objects
            firstLaunch = false
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.row)!")
        
        let model = self.viewModel.objects[indexPath.row]

        let alertController = UIAlertController(title: "Description", message: "Manufacturer - \(manufacturerName) and Model is - \(model.modelName)", preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
