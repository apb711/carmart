//
//  TableViewViewModel.swift
//  CarMart
//
//  Created by Adithi Bolar on 3/20/17.
//  Copyright © 2017 Adithi Bolar. All rights reserved.
//

import UIKit

protocol ReloadTableViewDelegate: class {
    func reloadTabelView()
}

class TableViewViewModel {
    var objects:[TableViewCellData] = []
    weak var reloadDelegate:ReloadTableViewDelegate?
    var dataDict:NSDictionary = [:]

}

extension TableViewViewModel: GetManufacturersDelegate {
    enum JSONError: String, Error {
        case NoData = "ERROR: no data"
        case ConversionFailed = "ERROR: conversion from JSON failed"
    }
}

extension TableViewViewModel {
    func gettingData(page: Int) {
        let urlString = "https://api-aws-eu-qa-1.auto1-test.com/v1/car-types/manufacturer?page=\(page)&pageSize=15&wa_key=coding-puzzle-client-449cc9d"
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with:url!) { (data, response, error) in
            if error != nil {
                print(error ?? "hello")
            } else {
                do {
                    
                    let parsedData = try JSONSerialization.jsonObject(with: data!, options: []) as! [NSString:Any]
                    self.dataDict = parsedData as NSDictionary
                    
                    print(self.dataDict["wkda"] ?? "Error")
                    
                    let dict = self.dataDict["wkda"] as! NSDictionary
                    
                    for (key, value) in dict {
                        print("key is - \(key) and value is - \(value)")
                        
                        guard let manName = value as? String else {
                            print("Error can't get country name")
                            return
                        }
                        
                        guard let manId = key as? String else {
                            print("Error can't get city name")
                            return
                        }
                        
                        let obj = TableViewCellData(manufacturerName: manName, manufacturerId: manId)
                        self.objects.append(obj)
                    }
                    
                    DispatchQueue.main.async {
                        self.reloadDelegate?.reloadTabelView()
                    }
                    
                } catch let error as NSError {
                    print(error)
                }
            }
            
            }.resume()
    }
}































