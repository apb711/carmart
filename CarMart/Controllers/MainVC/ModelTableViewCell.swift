//
//  ModelTableViewCell.swift
//  CarMart
//
//  Created by Adithi Bolar on 3/20/17.
//  Copyright © 2017 Adithi Bolar. All rights reserved.
//

import UIKit

protocol MyTextCellDataSource {
    var manufacturerName:String { get }
    var manufacturerId:String { get }
}

protocol MyTextCellParametrs {
    var myTextColor: UIColor { get }
    var myFont: UIFont { get }
}

protocol GetManufacturersDelegate: class {
}

struct TableViewCellData:MyTextCellDataSource {
    var manufacturerName: String
    var manufacturerId: String
}

extension MyTextCellParametrs  {
    var myTextColor: UIColor {
        return .black
    }
    
    var myFont: UIFont {
        return .systemFont(ofSize: 17)
    }
}

extension UILabel: MyTextCellParametrs {
    
    func setParametrs() {
        self.textColor = myTextColor
        self.font = myFont
    }
}


class ModelTableViewCell: UITableViewCell {
    @IBOutlet weak var manufacturerName: UILabel!
    
    private var dataSource: MyTextCellDataSource?
    private weak var delegate: GetManufacturersDelegate?

   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(dataSource: MyTextCellDataSource, delegate: GetManufacturersDelegate) {
        self.dataSource = dataSource
        self.delegate = delegate
        
        manufacturerName.setParametrs()
        
        manufacturerName.text = dataSource.manufacturerName
    }
}









